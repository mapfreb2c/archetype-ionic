import { Component, OnInit } from '@angular/core';
import { Platform, App } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TranslateService } from 'ng2-translate';

import { HomePage } from '../pages/home/home';
import { MenuOption } from "mapfre-framework";

@Component({
  templateUrl: 'app.html'
})
export class MyApp implements OnInit {
  rootPage: any = HomePage;
  menuOptions: MenuOption[];

  constructor(
    public app: App,
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public translate: TranslateService
  ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      translate.setDefaultLang('es');
      translate.use('es');

      console.log('platform ready');
    });

    window.datalayer.dispositivo = this.platform.platforms().join(" - ");
    app.viewDidEnter.subscribe(
      (view) => {
        console.log("View changed, calling datalayer");
        this.pageChange(view.instance.name);
      }
    );
  }

  pageChange(pageName: string): void {
    if (window.visualizarPaso) {
      window.datalayer.title = pageName;
      window.visualizarPaso({ type: 'pageview' });
    } else {
      setTimeout( () => {
        console.log("visualizarPaso is not defined. Waiting 0,5 s...");
        this.pageChange(pageName);
      }, 500);
    }
  }

  ngOnInit(): void {

    this.menuOptions = [
      {
        option: {
          icon: 'ios-apps-outline',
          text: 'INICIO',
        }
      },
      {
        option: {
          icon: 'ios-car-outline',
          text: 'COCHE',
          subText: 'Otros seguros de coche',
          subIcon: 'add'
        },
        subOptions: [
          {
            option: {
              text: 'Audi A1',
              subText: 'Todo riesgo con franquicia de 250 euros'
            },
            subOptions: [
              {
                option: {
                  text: 'Descargar documentacion'
                }
              },
              {
                option: {
                  text: 'Pagar un recibo'
                }
              },
              {
                option: {
                  text: 'Solicitar asistencia'
                }
              },
              {
                option: {
                  text: 'Dar un parte'
                }
              }
            ]
          },
          {
            option: {
              text: 'Audi A8',
              subText: 'Todo riesgo'
            },
            subOptions: [
              {
                option: {
                  text: 'Descargar documentacion'
                }
              },
              {
                option: {
                  text: 'Pagar un recibo'
                }
              },
              {
                option: {
                  text: 'Solicitar asistencia'
                }
              },
              {
                option: {
                  text: 'Dar un parte'
                }
              }
            ]
          }
        ]
      },
      {
        option: {
          icon: 'ios-home-outline',
          text: 'HOGAR',
          subText: 'Otros seguros de hogar',
          subIcon: 'add'
        },
        subOptions: [
          {
            option: {
              text: 'Casa de la playa',
              subText: 'Familiar'
            },
            subOptions: [
              {
                option: {
                  text: 'Descargar documentacion'
                }
              },
              {
                option: {
                  text: 'Pagar un recibo'
                }
              },
              {
                option: {
                  text: 'Solicitar asistencia'
                }
              },
              {
                option: {
                  text: 'Dar un parte'
                }
              }
            ]
          }
        ]
      },
      {
        option: {
          icon: 'ios-more-outline',
          text: 'OTROS SEGUROS'
        },
        subOptions: [
          {
            option: {
              text: 'Coche',
              icon: 'car'
            }
          },
          {
            option: {
              text: 'Hogar',
              icon: 'home'
            }
          },
          {
            option: {
              text: 'Moto',
              icon: 'bicycle'
            }
          },
          {
            option: {
              text: 'Salud',
              icon: 'flask'
            }
          },
          {
            option: {
              text: 'Decesos',
              icon: 'hand'
            }
          },
          {
            option: {
              text: 'Viaje y ocio',
              icon: 'globe'
            }
          },
          {
            option: {
              text: 'Vida',
              icon: 'heart'
            }
          },
          {
            option: {
              text: 'Otros',
              icon: 'more'
            }
          },
        ],
        type: 2
      },
    ];
  }

}
